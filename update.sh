#!/usr/bin/env bash
_cleanup=${CLEANUP:-14d}
function Bye {
  echo "${1}"
  exit 1
}

sudo httm -S .
sudo cp nixos/*.nix /etc/nixos/.
sudo mkdir -p /etc/nixos/secrets
sudo cp nixos/.sops.yaml /etc/nixos/.
sudo cp nixos/secrets/secrets.yaml /etc/nixos/secrets/.
sudo nixos-rebuild switch --upgrade --flake '/etc/nixos#mystra' || Bye "# Hmm rebuild faild; Exiting..."
sudo nix-collect-garbage --delete-older-than ${_cleanup}
sudo nixos-rebuild boot || Bye "boot rebuild failed; exiting..."
home-manager switch

