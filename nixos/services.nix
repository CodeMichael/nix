{ config, pkgs, ... }:
{
  services = {
    automatic-timezoned.enable = true;
    flatpak.enable = true;
    fprintd.enable = true;
    fwupd.enable = true;
    pcscd.enable = true;
    power-profiles-daemon.enable = true;
    pipewire = {
      alsa.enable = true;
      alsa.support32Bit = true;
      enable = true;
      jack.enable = true;
      pulse.enable = true;
    };
    sanoid = {
      enable = true;
      interval = "hourly";
      datasets = {
        "zroot/fw16/data" = {
          autoprune = true;
          autosnap = true;
          daily = 30;
          hourly = 24;
          monthly = 3;
          yearly = 1;
          recursive = true;
        };
      };
    };
    syncoid = {
      enable = true;
      commands."codemichael" = {
        source = "zroot/fw16/data/home/codemichael";
        target = "zfw16ext/backup/codemichael";
      };
    };
    tailscale = {
      enable = true;
      useRoutingFeatures = "client";
    };
    # tlp = {
    #   enable = true;
    #   settings = {
    #     CPU_SCALING_GOVERNOR_ON_AC = "performance";
    #     CPU_SCALING_GOVERNOR_ON_BAT = "powersave";

    #     CPU_ENERGY_PERF_POLICY_ON_BAT = "power";
    #     CPU_ENERGY_PERF_POLICY_ON_AC = "performance";

    #     CPU_MIN_PERF_ON_AC = 0;
    #     CPU_MAX_PERF_ON_AC = 100;
    #     CPU_MIN_PERF_ON_BAT = 0;
    #     CPU_MAX_PERF_ON_BAT = 20;

    #    #Optional helps save long term battery health
    #    START_CHARGE_THRESH_BAT0 = 40; # 40 and bellow it starts to charge
    #    STOP_CHARGE_THRESH_BAT0 = 80; # 80 and above it stops charging

    #   };
    # };
    udev.packages = [ pkgs.via ];
    xserver = {
      displayManager.gdm.enable = true;
      # desktopManager.plasma6.enable = true;
      desktopManager.gnome.enable = true;
      enable = true;
      videoDrivers = [ "amdgpu" ];
      xkb.layout = "us";
    };
    zfs = {
      trim.enable = true;
      autoScrub = {
        enable = true;
        pools = [ "zroot" ];
      };
      zed.settings = {
        ZED_PUSHOVER_TOKEN = "$(cat ${config.sops.secrets.pushover_token.path})";
        ZED_PUSHOVER_USER = "362BwfMGEbFd63UUg8upqExah1HvnH";
        ZED_NOTIFY_VERBOSE = "1";
      };
    };
  };
}
