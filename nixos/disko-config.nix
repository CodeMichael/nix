{
  disko.devices = {
    disk = {
      nv2230 = {
        type = "disk";
        # device = "/dev/disk/by-id/nvme-eui.e8238fa6bf530001001b448b4c0adec0";
        device = "/dev/nvme0n1";
        content = {
          type = "gpt";
          partitions = {
            ESP = {
              size = "500M";
              type = "EF00";
              content = {
                type = "filesystem";
                format = "vfat";
                mountpoint = "/boot2";
              };
            };
            swap = {
              size = "32G";
              content = {
                type = "mdraid";
                name = "swap";
              };
            };
            zroot = {
              size = "1T";
              content = {
                type = "zfs";
                pool = "zroot";
              };
            };
            zgames = {
              size = "100%";
              content = {
                type = "zfs";
                pool = "zgames";
              };
            };
          };
        };
      };
      nv2280 = {
        type = "disk";
        # device = "/dev/disk/by-id/nvme-eui.e8238fa6bf530001001b448b4a43c9b7";
        device = "/dev/nvme1n1";
        content = {
          type = "gpt";
          partitions = {
            ESP = {
              size = "500M";
              type = "EF00";
              content = {
                type = "filesystem";
                format = "vfat";
                mountpoint = "/boot";
              };
            };
            swap = {
              size = "32G";
              content = {
                type = "mdraid";
                name = "swap";
              };
            };
            zroot = {
              size = "1T";
              content = {
                type = "zfs";
                pool = "zroot";
              };
            };
            zgames = {
              size = "100%";
              content = {
                type = "zfs";
                pool = "zgames";
              };
            };
          };
        };
      };
    };
    mdadm = {
      swap = {
        type = "mdadm";
        level = 0;
        content = {
          type = "gpt";
          partitions = {
            primary = {
              size = "100%";
              content = {
                type = "swap";
                randomEncryption = true;
              };
            };
          };
        };
      };
    };
    zpool = {
      zroot = {
        type = "zpool";
        mode = "mirror";
        rootFsOptions = {
          compression = "zstd";
          "com.sun:auto-snapshot" = "false";
        };
        # mountpoint = "/";
        postCreateHook = "zfs snapshot zroot@blank";
        datasets = {
          fw16 = {
            type = "zfs_fs";
            mountpoint = "/";
            options."com.sun:auto-snapshot" = "false";
            options = {
              mountpoint = "/";
              encryption = "aes-256-gcm";
              keyformat = "passphrase";
            };
            postCreateHook = ''
              zfs set keylocation="prompt" "zroot/$name"
            '';
          };
          "fw16/data" = {
            type = "zfs_fs";
            # mountpoint = "/home";
            options."com.sun:auto-snapshot" = "true";
          };
          "fw16/data/home" = {
            type = "zfs_fs";
            mountpoint = "/home";
            options."com.sun:auto-snapshot" = "true";
          };
          "fw16/data/home/codemichael" = {
            type = "zfs_fs";
            mountpoint = "/home/codemichael";
            options."com.sun:auto-snapshot" = "true";
          };
          "fw16/data/root" = {
            type = "zfs_fs";
            mountpoint = "/root";
            options."com.sun:auto-snapshot" = "true";
          };
          "fw16/data/srv" = {
            type = "zfs_fs";
            mountpoint = "/srv";
            options."com.sun:auto-snapshot" = "true";
          };
          "fw16/var" = {
            type = "zfs_fs";
            mountpoint = "/var";
          };
          "fw16/var/lib/docker" = {
            type = "zfs_fs";
            mountpoint = "/var/lib/docker";
          };
          "fw16/var/lib/libvirt" = {
            type = "zfs_fs";
            mountpoint = "/var/lib/libvirt";
          };
        };
      };
      zgames = {
        type = "zpool";
        rootFsOptions = {
          compression = "zstd";
          "com.sun:auto-snapshot" = "false";
        };
        mountpoint = "/var/games";
        options.mountpoint = "legacy";
      };
    };
  };
}
