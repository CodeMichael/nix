{ config, pkgs, ... }:
{
  programs = {
    gnupg.agent.enable = true;
    mosh.enable = true;
    ssh.askPassword = pkgs.lib.mkForce "${pkgs.gnome.seahorse.out}/libexec/seahorse/ssh-askpass";
    neovim = {
      enable = true;
      defaultEditor = true;
    };
    zsh.enable = true;
  };
}
