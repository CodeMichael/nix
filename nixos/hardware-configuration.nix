{ config, lib, pkgs, modulesPath, ... }:

{
  imports =
    [
      (modulesPath + "/installer/scan/not-detected.nix")
    ];

  boot = {
    extraModprobeConfig = "options vfio-pci ids=1002:7480,1002:ab30";
    extraModulePackages = with config.boot.kernelPackages; [ kvmfr ];
    initrd = {
      availableKernelModules = [
        "amdgpu"
        "nvme"
        "xhci_pci"
        "thunderbolt"
        "usbhid"
        "usb_storage"
        "uas"
        "sd_mod"
        "pci_stub"
        "vfio"
        "vfio-pci"
        "vfio_iommu_type1"
      ];
      kernelModules = [
        "vfio"
        "vfio-pci"
        # order matters here, the vfio have to come before the amdgpu
        # or else the gpu will grab the pci device before it can be
        # set for vfio-pci passthru
        "amdgpu"
      ];
      preDeviceCommands = ''
        DEVS="0000:03:00.0 0000:03:00.1"
        for DEV in $DEVS; do
          echo "vfio-pci" > /sys/bus/pci/devices/$DEV/driver_override
        done
        modprobe -i vfio-pci
      '';
    };
    kernelModules = [
      "kvm-amd"
      "pci_stub"
      "vfio_pci"
      "vfio"
      "vfio_iommu_type1"
      "kvmfr"
    ];
    kernelParams = [
      "iommu=pt"
      # "pcie_aspm=off"
      "amd_iommu=on"
      "vfio-pci.ids=1002:7480,1002:ab30"
      "pci-stub.ids=1002:7480,1002:ab30"
      "mem_sleep_default=deep"
    ];
    kernelPackages = config.boot.zfs.package.latestCompatibleLinuxPackages;
    loader = {
      efi = {
        canTouchEfiVariables = true;
        efiSysMountPoint = "/boot";
      };
      grub = {
        enable = true;
        zfsSupport = true;
        efiSupport = true;
        mirroredBoots = [
          { devices = [ "nodev" ]; path = "/boot"; }
          { devices = [ "nodev" ]; path = "/boot2"; }
        ];
        # gfxmodeEfi = "2560x1600x32";
	# theme = "/boot/grub/themes/fw/theme.txt";
      };
      systemd-boot.enable = false;
    };
    supportedFilesystems = [ "zfs" ];
    zfs.allowHibernation = false;
  };
  sound.enable = true;
  hardware = {
    cpu.amd.updateMicrocode = lib.mkDefault config.hardware.enableRedistributableFirmware;
    keyboard.qmk.enable = true;
    opengl = {
      driSupport = true;
      driSupport32Bit = true;
      enable = true;
      extraPackages = with pkgs; [
        amdvlk
        libva
        libvdpau
        libvdpau-va-gl
        vaapiVdpau
      ];
      extraPackages32 = [
        pkgs.driversi686Linux.amdvlk
      ];
    };
    pulseaudio.enable = false;
    # steam-hardware.enable = true;
  };

  nixpkgs.hostPlatform = lib.mkDefault "x86_64-linux";
}
