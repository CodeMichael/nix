# Edit this configuration file to define what should be installed on
# your system. Help is available in the configuration.nix(5) man page, on
# https://search.nixos.org/options and in the NixOS manual (`nixos-help`).

{ config, lib, pkgs, ... }:

{
  imports =
    [ # Include the results of the hardware scan.
      # "${builtins.fetchTarball "https://github.com/nix-community/disko/archive/master.tar.gz"}/module.nix"
      ./disko-config.nix
      ./hardware-configuration.nix
      ./programs.nix
      ./services.nix
      ./users.nix
    ];
  sops = {
    defaultSopsFile = ./secrets/secrets.yaml;
    defaultSopsFormat = "yaml";
    age.keyFile = "/home/codemichael/.config/sops/age/keys.txt";
    secrets.pushover_token = { };
  };
  nix = {
    package = pkgs.nixFlakes;
    extraOptions = ''
      experimental-features = nix-command flakes
    '';
  };

  systemd.network = {
    enable = true;
    wait-online.enable = false;
  };
  networking = {
    hostId = "65c92507";
    hostName = "mystra";
    networkmanager.enable = true;
    useNetworkd = true;
  };

  time.timeZone = "America/Chicago";

  i18n.defaultLocale = "en_US.UTF-8";
  console = {
    font = "Lat2-Terminus16";
    useXkbConfig = true;
  };

  security.rtkit.enable = true;

  environment.systemPackages = with pkgs; [
    age
    docker
    home-manager
    httm
    linuxPackages.kvmfr
    looking-glass-client
    opensc
    qmk
    qmk-udev-rules
    sops
    vim
  ];

  # For more information, see `man configuration.nix` or https://nixos.org/manual/nixos/stable/options#opt-system.stateVersion .
  system = {
    stateVersion = "24.05";
    autoUpgrade.enable = true;
    autoUpgrade.allowReboot = false;
  };


  # all things virtual

  systemd.tmpfiles.rules = [
    "f /dev/shm/looking-glass 0660 codemichael qemu-libvirtd -"
  ];

  virtualisation = {
    docker = {
      enable = true;
      autoPrune = {
        enable = true;
        dates = "weekly";
      };
    };
    libvirtd = {
      enable = true;
      qemu = {
        swtpm.enable = true;
        ovmf.enable = true;
        ovmf.packages = [ pkgs.OVMFFull.fd ];
      };
    };
    spiceUSBRedirection.enable = true;
  };
}

