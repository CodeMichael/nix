{ config, pkgs, ... }:
{
  users.users.codemichael = {
    isNormalUser = true;
    home = "/home/codemichael";
    shell = pkgs.zsh;
    description = "Michael";
    extraGroups = [
      "docker"
      "libvirtd"
      "qemu-libvirtd"
      "wheel"
      "networkmanager"
      "plugdev"
    ];
  };
}
