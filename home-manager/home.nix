{ config, pkgs, lib, ... }:
let

  my_yubikey = "~/.ssh/id_ed25519_sk";

  python-with-packages = pkgs.python3.withPackages (pp: with pp; [
    jc
  ]);
  nixvim = import (builtins.fetchGit {
    url = "https://github.com/nix-community/nixvim";
    # If you are not running an unstable channel of nixpkgs, select the corresponding branch of nixvim.
    # ref = "nixos-23.05";
  });
in
{
  imports = [
    # For home-manager
    nixvim.homeManagerModules.nixvim
    # For NixOS
    # nixvim.nixosModules.nixvim
    # For nix-darwin
    # nixvim.nixDarwinModules.nixvim
  ];
  fonts.fontconfig.enable = true;
  # nixpkgs.config.allowUnfree = true;
  # nixpkgs.options.services.psd.enable = true;
  nixpkgs.config.allowUnfreePredicate = pkg: builtins.elem (lib.getName pkg) [
    "discord"
    "microsoft-edge"
    "microsoft-edge-stable"
    "obsidian"
    "plex-media-player"
    "plexamp"
    "plexamp-4.8.3"
    "steam"
    "steam-original"
    "steam-run"
    # "sublime4"
    # "sublimetext4-4169"
    "veracrypt"
    "vscode"
  ];
  nix = {
    enable = true;
    package = pkgs.nix;
    settings = {
      experimental-features = "nix-command flakes";
    };
  };
  # manual.manpages.enable = false;
  home = {
    file = {
      ".looking-glass-client.ini" = { source = ./looking-glass-client.ini; };
      ".local/bin" = {
        source = local/bin;
        recursive = true;
      };
      ".steam/steam/steam_dev.cfg" = { source = steam/steam_dev.cfg; };
    };
    username = "codemichael";
    homeDirectory = "/home/codemichael";
    stateVersion = "24.05";
    sessionPath = [
      "$HOME/.local/bin"
    ];
    sessionVariables = {
      EDITOR = "nvim";
      VISUAL = "nvim";
      MOZ_ENABLE_WAYLAND = 1;
      # QT_QPA_PLATFORM = "wayland";
      # NIXOS_OZONE_WL = 1;
    };
    packages = with pkgs; [
      bash
      bind
      bitwarden
      bitwarden-cli
      bitwarden-desktop
      bk # cli epub reader
      chrome-gnome-shell
      discord
      element-desktop
      eza
      firefox-wayland
      flatpak
      foot
      font-awesome
      gamemode
      glances
      glow
      glxinfo
      gnomeExtensions.appindicator
      gnomeExtensions.caffeine
      gnomeExtensions.gesture-improvements
      gnomeExtensions.gtile
      # gnomeExtensions.taildrop-send
      gnomeExtensions.tailscale-status
      gnome.dconf-editor
      gnome.gnome-tweaks
      gnome.gnome-software
      gnupg
      gping
      httm
      httpie
      itch
      jetbrains-mono
      krita
      # logseq
      nordic
      mangohud
      microsoft-edge
      mosh
      moonlight-qt
      mplayer
      nerdfonts
      newsflash
      nixpkgs-fmt
      nordic
      obsidian
      pciutils
      pinentry-gnome
      plex-media-player
      plexamp
      powerline
      powerline-fonts
      powertop
      profile-sync-daemon
      #protonmail-bridge
      protonvpn-cli
      protonvpn-gui
      python-with-packages
      pwgen
      repgrep
      ranger
      ripgrep
      ripgrep-all
      sanoid
      signal-desktop
      steam
      syncthingtray
      # sublime4
      tor-browser-bundle-bin
      thunderbird
      ugrep
      unzip
      usbutils
      yubikey-manager
      yubikey-manager-qt
      yubikey-personalization
      yubikey-personalization-gui
      #yubioath-desktop
      yubioath-flutter
      ventoy-full
      veracrypt
      via
      virt-manager
      vscode
      (pkgs.vim_configurable.customize {
        name = "vim";
        vimrcConfig = {
          customRC = ''
            set number
            call plug#begin()
            Plug 'LnL7/vim-nix'
            call plug#end()
          '';
          plug.plugins = with pkgs.vimPlugins ; [ vim-nix ];
        };
      })
    ];
  };
  services = {
    gpg-agent = {
      pinentryFlavor = "curses";
      enableSshSupport = true;
      enableExtraSocket = true;
      extraConfig = ''
        pinentry-program ${pkgs.pinentry-curses}/bin/pinentry-curses
      '';
    };
    # profile-sync-daemon.enable = true;
    # psd.enable = true;
    syncthing = {
      enable = true;
      tray.enable = true;
    };
  };
  systemd.user.sessionVariables.SSH_AUTH_SOCK = "/run/user/1000/keyring/ssh";
  programs = {

    alacritty = {
      enable = true;
    };
    bat = {
      enable = true;
      config = { theme = "Nord"; style = "grid"; };
    };
    btop = {
      enable = true;
      settings.color_theme = "nord";
    };
    git = {
      enable = true;
      userName = "codemichael";
      userEmail = "codemichael@codemichael.net";
      # signing = {
      #   key = "BE733E40433A84BD";
      #   signByDefault = true;
      # };
      extraConfig = {
        commit.gpgsign = true;
        gpg.format = "ssh";
        user.signingkey = "${my_yubikey}.pub";
      };
    };
    gpg = {
      enable = true;
      # publicKeys = [{ source = ./codemichael.gpg.pub.key; }];
    };
    kitty = {
      enable = true;
      font = {
        name = "JetBrains Mono";
      };
      settings = {
        font_size = "16.0";
        # hide_window_decorations = "yes";
        dynamic_background_opacity = "yes";
        background_opacity = "0.9";
        term = "xterm-256color";
        wayland_titlebar_color = "background";
        tab_title_template = "{index}: {title}";
      };
      extraConfig = ''
        # Nord Colorscheme for Kitty
        # Based on:
        # - https://gist.github.com/marcusramberg/64010234c95a93d953e8c79fdaf94192
        # - https://github.com/arcticicestudio/nord-hyper

        foreground            #D8DEE9
        background            #2E3440
        selection_foreground  #000000
        selection_background  #FFFACD
        url_color             #0087BD
        cursor                #81A1C1

        # black
        color0   #3B4252
        color8   #4C566A

        # red
        color1   #BF616A
        color9   #BF616A

        # green
        color2   #A3BE8C
        color10  #A3BE8C

        # yellow
        color3   #EBCB8B
        color11  #EBCB8B

        # blue
        color4  #81A1C1
        color12 #81A1C1

        # magenta
        color5   #B48EAD
        color13  #B48EAD

        # cyan
        color6   #88C0D0
        color14  #8FBCBB

        # white
        color7   #E5E9F0
        color15  #ECEFF4

      '';
    };
    nixvim = {
      enable = true;
      clipboard.providers.wl-copy.enable = true;
      colorschemes.nord.enable = true;
      plugins.barbar.enable = true;
      plugins.comment.enable = true; #keyboard shortcut: gcc
      plugins.gitsigns.enable = true;
      plugins.neogit.enable = true;
      plugins.nvim-tree.enable = true;
      plugins.nvim-tree.openOnSetup = true;
      plugins.nvim-tree.openOnSetupFile = true;
      # plugins.nvim-tree.options.followCurrentFile.enabled = true;
      # plugins.rainbow-delimiters.enable = true;
      opts = {
        number = true; # Show line numbers
        # relativenumber = true; # Show relative line numbers
        shiftwidth = 2; # Tab width should be 2
      };
    };
    ssh = {
      enable = true;
      matchBlocks = {
        "gitlab.com" = {
          identityFile = "${my_yubikey}";
          extraOptions = {
            PreferredAuthentications = "publickey";
          };
        };
      };
    };
    starship = {
      enable = true;
      settings = { };
    };
    tmux = {
      enable = true;
      clock24 = true;
      terminal = "screen-256color";
      plugins = with pkgs; [ tmuxPlugins.nord ];
      sensibleOnTop = false;
      extraConfig = ''
        # source ${pkgs.powerline}/share/tmux/powerline.conf
      '';
    };
    vscode = {
      enable = true;
      # package = pkgs.vscodium;    # You can skip this if you want to use the unfree version
      extensions = with pkgs.vscode-extensions; [
        arcticicestudio.nord-visual-studio-code
        bbenoist.nix
        codezombiech.gitignore
        davidanson.vscode-markdownlint
        # redhat.ansible
        # hediet.vscode-drawio
        # wayou.vscode-todo-highlight
        gruntfuggly.todo-tree
        gitlab.gitlab-workflow
        hashicorp.terraform
        jnoortheen.nix-ide
        ms-azuretools.vscode-docker
        oderwat.indent-rainbow
        redhat.vscode-xml
        redhat.vscode-yaml
        vscodevim.vim
        yzhang.markdown-all-in-one
      ];
      userSettings = {
        "vim.handleKeys" = {
          "<C-d>" = true;
          "<C-c>" = false;
          "<C-v>" = false;
          "<C-x>" = false;
          "<C-a>" = false;
          "<C-f>" = false;
          "<C-n>" = false;
        };
        "files.associations" = {
          "*.nomad" = "terraform";
          "*.hcl" = "terraform";
          "*.nomad.j2" = "terraform";
          "*.hcl.j2" = "terraform";
          "*.yml" = "ansible";
        };
        "yaml.customTags" = [
          "!vault"
        ];
        "terminal.integrated.fontFamily" = "Hack Nerd Font";
        "workbench.preferredDarkColorTheme" = "Nord";
        "workbench.colorTheme" = "Nord";
        "editor.tabSize" = 2;
      };
    };
    zellij = {
      enable = true;
      settings = {
        theme = "nord";
        pane_frames = false;
      };
    };
    zsh = {
      enable = true;
      initExtra = ''
        # eval $(ssh-agent) > /dev/null
        # export GPG_TTY="$(tty)"
        # eval "$(starship init zsh)"
        DISABLE_AUTO_TITLE="true"
      '';
      shellAliases = {
        ls = "eza --long --git --header --group --dereference --binary";
        # ssh = "mosh";
      };
      sessionVariables = {
        EDITOR = "nvim";
        VISUAL = "nvim";
        MOZ_ENABLE_WAYLAND = 1;
        # NIXOS_OZONE_WL = 1;
      };
      prezto = {
        # https://github.com/nix-community/home-manager/blob/master/modules/programs/zsh/prezto.nix
        enable = true;
        prompt.theme = "powerlevel10k";
        pmodules = [
          "archive"
          "autosuggestions"
          "completion"
          # "directory"
          "editor"
          # "environment"
          "git"
          "history"
          "history-substring-search"
          "prompt"
          "python"
          # "spectrum"
          # "ssh"
          "syntax-highlighting"
          # "terminal"
          # "tmux"
          "utility"
        ];
        extraConfig = ''
          source ~/.p10k.zsh
        '';
      };
    };
  };
}
