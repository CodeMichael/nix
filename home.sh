#!/usr/bin/env bash

sudo httm -S .
sudo nix-channel --update home-manager
cp -r home-manager ~/.config
home-manager switch
