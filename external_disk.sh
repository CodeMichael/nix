zpool create -f \
	-o ashift=12 \
	-o autotrim=on \
	-O acltype=posixacl \
	-O canmount=on \
	-O dnodesize=auto \
	-O normalization=formD \
	-O relatime=on \
	-O xattr=sa \
	-O mountpoint=none \
	-O canmount=off \
	-O compression=zstd \
	-O encryption=aes-256-gcm \
	-O keyformat=passphrase   \
	-O keylocation=prompt     \
	zfw16ext \
	"usb-FRMW_1TB_Card_071C378BA20E4C08-0:0"
zfs create -o mountpoint=none zfw16ext/data
zfs create -o mountpoint=/mnt/fw16ext zfw16ext/data/codemichael
