# My install NixOS on FW16

## TL;DR

1. created workspace on external disk with `external_disk.sh`
2. created disk layout `disko-config.nix`
3. applied disk layout with `disko.sh`
4. installed system with `install.sh`
5. rebooted
6. made lots of changes and ran `update.sh`
7. Updated to using a flake

## Details

### Windows VM with dgpu passthru

Thanks to [Alexander Bakker][alexbakker] and [Astrid][astrid] for their blog entries about this exact topic, alot of this detail is a recitation of the same details.

#### Getting NixOS ready

Heres the simple config to get the emulation software ready to go. Libvirt and QEMU, the ovmf is necessary for UEFI emulation, IIRC.

```nix
environment.systemPackages = with pkgs; [
  virtmanager
];
virtualisation = {
  libvirtd = {
    enable = true;
    qemu = {
      swtpm.enable = true;
      ovmf.enable = true;
      ovmf.packages = [ pkgs.OVMFFull.fd ];
    };
  };
  spiceUSBRedirection.enable = true;
};
```

There a lot of little fiddly bits about getting the passthru working correctly, I'll go slow here, but you can go look at the files, mostly in `hardware-configuration.nix`. Firstly you'll need to get the information about your dGPU: its PCI address and the device id. You can run the following script to list out your IOMMU groups, and you should see your dGPU listed along with its audio device. Copy down the PCI address and device id for each. It should look something like the comments below.

```shell
#!/usr/bin/env bash
shopt -s nullglob
for g in `find /sys/kernel/iommu_groups/* -maxdepth 0 -type d | sort -V`; do
    echo "IOMMU Group ${g##*/}:"
    for d in $g/devices/*; do
        echo -e "\t$(lspci -nns ${d##*/})"
    done;
done;

## look for VGA compatible device matching your dGPU
## along with its partner audio device
## the PCI addresses will look like 
### 0000:03:00.0
## and the device id will look like
### 1002:7480
```

Now we can configure our system to take those devices and set them aside, during boot, for virtualization. That means that the dGPU will not be availble to the host, normally, after you set up this configuration. It is possible to re-attach the device to the host, but be aware, in the case of the framework, the amdgpu kernel module does not like being unloaded over and over. I was not able to successfully detach the amdgpu kernel driver without making the system unstable. As long as you're only passing it to virtual machines there are no instability issues that I am aware of.

For the device segregation there are couple of specific configurations (note below is just a highlighting of needed items, not a complete config). We need to make sure to load the vfio modules and to get the dGPU using them before the amdgpu module can grab them by default. We also need to make sure IOMMU support is enabled.

```nix
boot.extraModprobeConfig = "options vfio-pci ids=1002:7480,1002:ab30";
boot = {
  initrd = {
    availableKernelModules = [
      "pci_stub"
      "vfio"
      "vfio-pci"
      "vfio_iommu_type1"
      "amdgpu"
    ];
    kernelModules = [
      "vfio"
      "vfio-pci"
      # order matters here, the vfio have to come before the amdgpu
      # or else the gpu will grab the pci device before it can be
      # set for vfio-pci passthru
      "amdgpu"
    ];
    preDeviceCommands = ''
      DEVS="0000:03:00.0 0000:03:00.1"
      for DEV in $DEVS; do
        echo "vfio-pci" > /sys/bus/pci/devices/$DEV/driver_override
      done
      modprobe -i vfio-pci
    '';
  };
  kernelModules = [
    "kvm-amd"
    "pci_stub"
    "vfio_pci"
    "vfio"
    "vfio_iommu_type1"
  ];
  kernelParams = [
    "iommu=pt"
    "amd_iommu=on"
    "vfio-pci.ids=1002:7480,1002:ab30"
    "pci-stub.ids=1002:7480,1002:ab30"
    "mem_sleep_default=deep"
  ];
};
```

For best passthru client performance `looking-glass` is the way to go. We need to do some special bits to get the kvmfr module working optimally. Just make sure your user is part of the same group as the device.

```nix
boot.extraModulePackages = with config.boot.kernelPackages; [ kvmfr ];
boot.kernelModules = [ "kvmfr" ]
systemd.tmpfiles.rules = [
  "f /dev/shm/looking-glass 0660 root qemu-libvirtd -"
];
```

If, like me, you want to put windows onto a usb drive then the best strategy is to use [Rufus][rufus] to build a Windows-to-Go install, so that you could choose to boot it natively too. I'm not sure if theres a good/easy way to take an existing install and make it WTG, let me know if there is. Rufus can also do the download of the ISO for you, so highly recommended. Rufus is Windows-only, afaik, so chicken egg problem, you figure it out. Rufus can remove the requirement for TPM 2.0, or you can add a virtualized device.

```xml
<tpm model="tpm-crb">
  <backend type="emulator" version="2.0"/>
</tpm>
```

You'll want the following details in your libvirt xml that will allow you install the AMD drivers, as AMD doesn't normally want to be installed in a VM.

```xml
<features>
  <hyperv mode="custom">
    <vendor_id state="on" value="someRandomBits"/>
  </hyperv>
  <kvm> <hidden state="on"/> </kvm>
</features>
```

You'll also want to add the memory device for looking-glass. You can use a smaller number than 128, but if you're going to use a larger display a larger memory size is going to be needed.

```xml
<devices>
  <shmem name="looking-glass">
    <model type="ivshmem-plain"/>
    <size unit="M">128</size>
    <address type="pci" domain="0x0000" bus="0x10" slot="0x01" function="0x0"/>
  </shmem>
</devices>
```

Add a spice devices: video, audio, keyboard, and mouse

```xml
<devices>
  <input type="keyboard" bus="virtio"/>
  <input type="mouse" bus="virtio"/>
  <audio id="1" type="spice"/>
  <video>
    <model type="virtio" heads="1" primary="yes"/>
  </video>
  <graphics type="spice" port="5900" autoport="no">
    <listen type="address"/>
    <image compression="off"/>
  </graphics>
</devices>
```

To get the display output that you want, customized to your preference you can install a [virtual display driver][vdd]. Follow the instructions carefully (make sure to install the certificate) then set up your options file with your display resolution preferences (including refresh rate).

After you've got all this setup you should be able to install the AMD driver along with the looking glass host software and then you can use the looking glass client on your device without needing extra keyboards, mice, or displays.

## Links

- [Disko][disko] was used for disk management (mostly)


<!-- Links -->

[disko]: https://github.com/nix-community/disko "Disko -- Declarative disk partitioning"
[disko-boot-raid1]: https://github.com/nix-community/disko/blob/master/example/boot-raid1.nix
[example]: https://github.com/Lillecarl/nixos/blob/master/hosts/shitbox/disko.nix
[framework]: https://github.com/NixOS/nixos-hardware/tree/master/framework/13-inch/7040-amd
[rufus]: https://rufus.ie/en/ "Create bootable USB drives the easy way"
[alexbakker]: https://alexbakker.me/post/nixos-pci-passthrough-qemu-vfio.html "Notes on PCI Passthrough on NixOS using QEMU and VFIO"
[astrid]: https://astrid.tech/2022/09/22/0/nixos-gpu-vfio/ "A GPU Passthrough Setup for NixOS (with VR passthrough too!)"
[vdd]: https://github.com/itsmikethetech/Virtual-Display-Driver "Add virtual monitors to your windows 10/11 device! Works with VR, OBS, Sunshine, and/or any desktop sharing software."
